set (
    dep_catch2_source "vendor" CACHE STRING
    "Source to use for the catch2 dependency."
)

set (
    dep_catch2_git_tag "9e1bdca4667295fcb16265eae00efa8423f07007" CACHE STRING
    "Git tag to use for the catch2 dependency."
)

set (
    dep_catch2_git_shallow TRUE CACHE BOOL
    "Do a shallow git clone for the catch2 dependency."
)

set_property (CACHE dep_catch2_source PROPERTY STRINGS "vendor;system;git")

if (ENABLE_TESTS)
    if (dep_catch2_source STREQUAL "vendor")
        create_header_only_target (
            TARGET              Catch2::Catch2
            INCLUDE_DIRECTORIES "${CMAKE_SOURCE_DIR}/external/catch2/include"
        )

    elseif (dep_catch2_source STREQUAL "system")
        find_package (Catch2)

    elseif (dep_catch2_source STREQUAL "git")
        create_cmake_git_target (
            catch2
                REPOSITORY  "https://github.com/catchorg/Catch2.git"
                TAG         "${dep_catch2_git_tag}"
                SHALLOW     "${dep_catch2_git_shallow}"
        )

    else ()
        message (FATAL_ERROR "'${dep_catch2_source}' is not a valid option for dep_catch2_source.")
    endif ()

    check_valid_target (Catch2::Catch2 "Failed to find/retrieve FMT (${dep_fmt_source}) dependancy.")
endif ()
