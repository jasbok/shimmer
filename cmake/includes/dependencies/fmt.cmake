set (
    dep_fmt_source "vendor" CACHE STRING
    "Source to use for the fmt dependency."
)

set (
    dep_fmt_git_tag "3e75ad9822980e41bc591938f26548f24eb88907" CACHE STRING
    "Git tag to use for the fmt dependency."
)

set (
    dep_fmt_git_shallow TRUE CACHE BOOL
    "Do a shallow git clone for the fmt dependency."
)

set (
    dep_fmt_header_only TRUE CACHE BOOL
    "Use the fmt dependency as a header-only library (system,git)."
)

set_property (CACHE dep_fmt_source PROPERTY STRINGS "vendor;system;git")

if (dep_fmt_source STREQUAL "vendor")
    create_target (
        TARGET              fmt::fmt
        INCLUDE_DIRECTORIES "${CMAKE_SOURCE_DIR}/external/fmt/include"
        SRC_DIRECTORIES     "${CMAKE_SOURCE_DIR}/external/fmt/src"
    )

elseif (dep_fmt_source STREQUAL "system")
    find_package (fmt)

elseif (dep_fmt_source STREQUAL "git")
    create_cmake_git_target (
        fmt
            REPOSITORY  "https://github.com/fmtlib/fmt.git"
            TAG         "${dep_fmt_git_tag}"
            SHALLOW     "${dep_fmt_git_shallow}"
    )

else ()
    message (FATAL_ERROR "'${dep_fmt_source}' is not a valid option for dep_fmt_source.")
endif ()

check_valid_target (fmt::fmt "Failed to find/retrieve FMT (${dep_fmt_source}) dependancy.")

if (dep_fmt_source STREQUAL "git")
    if (dep_fmt_header_only)
        add_library (fmt::fmt ALIAS fmt-header-only)
    else ()
        set_target_properties (
            fmt
                PROPERTIES
                    LIBRARY_OUTPUT_DIRECTORY     "${BUILD_LIBS_DIR}"
                    COMPILE_FLAGS                "${BUILD_COMPILE_FLAGS}"
                    LINK_FLAGS                   "${BUILD_LINK_FLAGS}"
                    INSTALL_RPATH                "${INSTALL_LIBS_DIR}"
                    INSTALL_RPATH_USE_LINK_PATH  TRUE
                    POSITION_INDEPENDENT_CODE ON )
    endif ()
endif ()
