set (
    dep_glm_source "vendor" CACHE STRING
    "Source to use for the glm dependency."
)

set (
    dep_glm_git_tag "21a30e11164c6ffc1ec12811cf7ce82c9f3b2d1f" CACHE STRING
    "Git tag to use for the glm dependency."
)

set (
    dep_glm_git_shallow TRUE CACHE BOOL
    "Do a shallow git clone for the glm dependency."
)

set_property (CACHE dep_glm_source PROPERTY STRINGS "vendor;system;git")

if (dep_glm_source STREQUAL "vendor")
    create_header_only_target (
        TARGET              GLM::GLM
        INCLUDE_DIRECTORIES "${CMAKE_SOURCE_DIR}/external/glm/include"
    )

elseif (dep_glm_source STREQUAL "system")
    find_package (glm)

elseif (dep_glm_source STREQUAL "git")
    create_cmake_git_target (
        glm
            REPOSITORY  "https://github.com/g-truc/glm.git"
            TAG         "${dep_glm_git_tag}"
            SHALLOW     "${dep_glm_git_shallow}"
    )

else ()
    message (FATAL_ERROR "'${dep_glm_source}' is not a valid option for dep_glm_source.")
endif ()

if (NOT TARGET GLM::GLM)
    create_header_only_target (
        TARGET              GLM::GLM
        INCLUDE_DIRECTORIES "${GLM_INCLUDE_DIRS}"
    )
endif ()

check_valid_target (GLM::GLM "Failed to find/retrieve glm (${dep_glm_source}) dependancy.")
