set (
    dep_nlohmann_json_source "vendor" CACHE STRING
    "Source to use for the nlohmann::json dependency."
)

set (
    dep_nlohmann_json_git_tag "v3.3.0" CACHE STRING
    "Git tag to use for the nlohmann::json dependency."
)

set (
    dep_nlohmann_json_git_shallow TRUE CACHE BOOL
    "Do a shallow git clone for the nlohmann::json dependency."
)

set_property (CACHE dep_nlohmann_json_source PROPERTY STRINGS "vendor;system;git")

if (dep_nlohmann_json_source STREQUAL "vendor")
    create_header_only_target (
        TARGET              nlohmann_json::nlohmann_json
        INCLUDE_DIRECTORIES "${CMAKE_SOURCE_DIR}/external/nlohmann/include"
    )

elseif (dep_nlohmann_json_source STREQUAL "system")
    find_package (nlohmann_json)

elseif (dep_nlohmann_json_source STREQUAL "git")
    create_cmake_git_target (
        nlohmann_json
            REPOSITORY  "https://github.com/nlohmann/json.git"
            TAG         "${dep_nlohmann_json_git_tag}"
            SHALLOW     "${dep_nlohmann_json_git_shallow}"
    )

else ()
    message (FATAL_ERROR "'${dep_nlohmann_json_source}' is not a valid option for dep_nlohmann_json_source.")
endif ()

check_valid_target (nlohmann_json::nlohmann_json "Failed to find/retrieve nlohmann::json (${dep_nlohmann_json_source}) dependancy.")
