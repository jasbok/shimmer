find_package(SDL)

add_library(SDL::SDL IMPORTED INTERFACE)

set_target_properties(
    SDL::SDL
        PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${SDL_INCLUDE_DIR}"
            INTERFACE_LINK_LIBRARIES "${SDL_LIBRARY}" )

check_valid_target (SDL::SDL)
