set (
    dep_stb_source "vendor" CACHE STRING
    "Source to use for the stb dependency."
)

set (
    dep_stb_git_tag "e6afb9cbae4064da8c3e69af3ff5c4629579c1d2" CACHE STRING
    "Git tag/commit to use for the stb dependency."
)

set (
    dep_stb_git_shallow TRUE CACHE BOOL
    "Do a shallow git clone for the stb dependency."
)

set_property (CACHE dep_stb_source PROPERTY STRINGS "vendor;git")

if (dep_stb_source STREQUAL "vendor")
    create_header_only_target (
        TARGET              STB::STB
        INCLUDE_DIRECTORIES "${CMAKE_SOURCE_DIR}/external/stb/include"
    )

elseif (dep_stb_source STREQUAL "git")
    create_header_only_git_target (
        stb
            TARGET              STB::STB
            REPOSITORY          "https://github.com/nothings/stb.git"
            TAG                 "${dep_stb_git_tag}"
            SHALLOW             "${dep_stb_git_shallow}"
    )

else ()
    message (FATAL_ERROR "'${dep_stb_source}' is not a valid option for dep_stb_source.")
endif ()

check_valid_target (STB::STB "Failed to find/retrieve stb (${dep_stb_source}) dependancy.")
