option(ENABLE_EXAMPLES "Build and enable project examples." OFF)

if(ENABLE_EXAMPLES)
    add_subdirectory("${CMAKE_SOURCE_DIR}/glpp/example")
endif()
