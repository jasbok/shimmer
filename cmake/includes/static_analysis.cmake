###############################################################################
# clang-tidy
###############################################################################
option(ENABLE_CLANG_TIDY "Enable clang-tidy analysis on build." OFF)

set(CLANG_TIDY_BIN "clang-tidy"
    CACHE FILEPATH "Path to the clang-tidy binary." )

if(ENABLE_CLANG_TIDY)
    set_property(
        TARGET
            common
            glpp
            shimmer
            shimmer_sdl
            shimmer_sdl2
        PROPERTY
            CXX_CLANG_TIDY "${CLANG_TIDY_BIN}"
                -quiet
                -checks=bugprone-*,modernize-*,performance-*,-modernize-pass-by-value )
endif(ENABLE_CLANG_TIDY)


###############################################################################
# cppcheck
###############################################################################
option(ENABLE_CPPCHECK "Enable cppcheck analysis on build." OFF)

set(CPPCHECK_BIN "cppcheck"
    CACHE FILEPATH "Path to the cppcheck binary." )

if(ENABLE_CPPCHECK)
    set_property(
        TARGET
            common
            glpp
            shimmer
            shimmer_sdl
            shimmer_sdl2
        PROPERTY
            CXX_CPPCHECK "${CPPCHECK_BIN}"
                --quiet
                --std=c++14
                --enable=warning,style,performance,portability
                --suppress=*:${CMAKE_SOURCE_DIR}/external/fmt/fmt/format.h
                --suppress=*:${CMAKE_SOURCE_DIR}/external/fmt/fmt/time.h
                "--template={file}:{line}:{column}: {severity}: {message}"
                "--template-location={file}:{line}:{column}: note: {info} {code}" )
endif(ENABLE_CPPCHECK)


###############################################################################
# cpplint
###############################################################################
option(ENABLE_CPPLINT "Enable cpplint analysis on build." OFF)

set(CPPLINT_BIN "cpplint"
    CACHE FILEPATH "Path to the cpplint binary." )

if(ENABLE_CPPLINT)
    set_property(
        TARGET
            common
            glpp
            shimmer
            shimmer_sdl
            shimmer_sdl2
        PROPERTY
            CXX_CPPLINT "${CPPLINT_BIN}")
endif(ENABLE_CPPLINT)


###############################################################################
# include-what-you-use
###############################################################################
option(
    ENABLE_INCLUDE_WHAT_YOU_USE
    "Enable include-what-you-use analysis on build."
    OFF)

set(INCLUDE_WHAT_YOU_USE_BIN "iwyu"
    CACHE FILEPATH "Path to the include-what-you-use binary." )

if(ENABLE_INCLUDE_WHAT_YOU_USE)
    set_property(
        TARGET
            common
            glpp
            shimmer
            shimmer_sdl
            shimmer_sdl2
        PROPERTY
            CXX_INCLUDE_WHAT_YOU_USE "${INCLUDE_WHAT_YOU_USE_BIN}"
                --transitive_includes_only )
endif(ENABLE_INCLUDE_WHAT_YOU_USE)


###############################################################################
# link-what-you-use
###############################################################################
option(
    ENABLE_LINK_WHAT_YOU_USE
    "Enable link-what-you-use analysis on build."
    OFF)

set_property(
    TARGET
        common
        glpp
        shimmer
        shimmer_sdl
        shimmer_sdl2
    PROPERTY
        LINK_WHAT_YOU_USE ${ENABLE_LINK_WHAT_YOU_USE} )
