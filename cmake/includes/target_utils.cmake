# Get all propreties that cmake supports
# https://stackoverflow.com/questions/32183975/how-to-print-all-the-properties-of-a-target-in-cmake
execute_process (COMMAND cmake --help-property-list OUTPUT_VARIABLE CMAKE_PROPERTY_LIST)

STRING (REGEX REPLACE ";" "\\\\;" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")
STRING (REGEX REPLACE "\n" ";" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")
list (FILTER CMAKE_PROPERTY_LIST EXCLUDE REGEX "^LOCATION$|^LOCATION_|_LOCATION$")
list (REMOVE_DUPLICATES CMAKE_PROPERTY_LIST)

unset (CMAKE_WHITELISTED_PROPERTY_LIST)
foreach (prop ${CMAKE_PROPERTY_LIST})
    if (prop MATCHES "^(INTERFACE|[_a-z]|IMPORTED_LIBNAME_|MAP_IMPORTED_CONFIG_)|^(COMPATIBLE_INTERFACE_(BOOL|NUMBER_MAX|NUMBER_MIN|STRING)|EXPORT_NAME|IMPORTED(_GLOBAL|_CONFIGURATIONS|_LIBNAME)?|NAME|TYPE|NO_SYSTEM_FROM_IMPORTED)$")
        list (APPEND CMAKE_WHITELISTED_PROPERTY_LIST ${prop})
    endif ()
endforeach (prop)



function (create_header_only_target)
    cmake_parse_arguments (ARG "" "TARGET;INCLUDE_DIRECTORIES" "" ${ARGN})

    add_library (${ARG_TARGET} IMPORTED INTERFACE)

    set_target_properties (
        ${ARG_TARGET}
            PROPERTIES
                INTERFACE_INCLUDE_DIRECTORIES "${ARG_INCLUDE_DIRECTORIES}"
    )

endfunction ()

function (create_target)
    cmake_parse_arguments (ARG "" "TARGET;SRC_DIRECTORIES;INCLUDE_DIRECTORIES" "" ${ARGN})

    set (target_srcs "")
    foreach (SRC_DIR ${ARG_SRC_DIRECTORIES})
        file (GLOB srcs "${SRC_DIR}/*\.c*")
        list (APPEND target_srcs ${srcs})
    endforeach ()

    add_library (${ARG_TARGET} IMPORTED INTERFACE)

    target_sources (${ARG_TARGET} INTERFACE ${target_srcs})

    set_target_properties (
        ${ARG_TARGET}
            PROPERTIES
                INTERFACE_INCLUDE_DIRECTORIES "${ARG_INCLUDE_DIRECTORIES}"
    )

endfunction ()

function (create_cmake_git_target NAME)
    cmake_parse_arguments (ARG "" "REPOSITORY;TAG;SHALLOW" "" ${ARGN})

    FetchContent_Declare (
        ${NAME}
            GIT_REPOSITORY  "${ARG_REPOSITORY}"
            GIT_TAG         "${ARG_TAG}"
            GIT_SHALLOW     ${ARG_SHALLOW}
            GIT_PROGRESS    TRUE
    )

    FetchContent_GetProperties (${NAME})

    if (NOT ${NAME}_POPULATED)
      message ("Fetching '${NAME}:${ARG_TAG}' from git repository: ${ARG_REPOSITORY} (Shallow: ${ARG_SHALLOW})")

      FetchContent_Populate (${NAME})
      add_subdirectory (${${NAME}_SOURCE_DIR} ${${NAME}_BINARY_DIR})
    endif ()

endfunction ()

function (create_cmake_url_archive_target NAME)
    cmake_parse_arguments(ARG "" "URL;URL_HASH" "" ${ARGN})

    FetchContent_Declare (
        ${NAME}
            URL "${ARG_URL}"
    )

    FetchContent_GetProperties (${NAME})

    if (NOT ${NAME}_POPULATED)
      message ("Fetching '${NAME}' from URL: ${ARG_URL}")

      FetchContent_Populate (${NAME})

      add_subdirectory (${${NAME}_SOURCE_DIR} ${${NAME}_BINARY_DIR})
    endif ()

endfunction ()

function (create_header_only_url_target NAME)
    cmake_parse_arguments (ARG "" "TARGET;URL;URL_HASH" "" ${ARGN})

    FetchContent_Declare (
        ${NAME}
            URL                 "${ARG_URL}"
            DOWNLOAD_NO_EXTRACT TRUE
    )

    FetchContent_GetProperties (${NAME})

    if (NOT ${NAME}_POPULATED)
        message ("Fetching ${NAME} header from url: ${ARG_URL}")

        FetchContent_Populate (${NAME})

        create_header_only_target (
            TARGET                ${ARG_TARGET}
            INCLUDE_DIRECTORIES   "${${NAME}_SOURCE_DIR}"
        )
    endif ()

endfunction ()


function (create_header_only_git_target NAME)
    cmake_parse_arguments (ARG "" "TARGET;REPOSITORY;TAG;SHALLOW" "INCLUDE_DIRECTORY" ${ARGN})

    FetchContent_Declare (
        ${NAME}
            GIT_REPOSITORY  "${ARG_REPOSITORY}"
            GIT_TAG         "${ARG_TAG}"
            GIT_SHALLOW     ${ARG_SHALLOW}
            GIT_PROGRESS    TRUE
    )

    FetchContent_GetProperties (${NAME})

    if (NOT ${NAME}_POPULATED)
        message ("Fetching '${NAME}:${ARG_TAG}' from git repository: ${ARG_REPOSITORY} (Shallow: ${ARG_SHALLOW})")

        FetchContent_Populate (${NAME})

        set (include_dir "${CMAKE_CURRENT_BINARY_DIR}/_deps/_${NAME}")
        file (GLOB FILES_H "${${NAME}_SOURCE_DIR}/${INCLUDE_DIRECTORY}/*\.h")
        file (GLOB FILES_HPP "${${NAME}_SOURCE_DIR}/${INCLUDE_DIRECTORY}/*\.hpp")
        file (COPY ${FILES_H} ${FILES_HPP} DESTINATION "${include_dir}/${NAME}")

        create_header_only_target (
            TARGET                ${ARG_TARGET}
            INCLUDE_DIRECTORIES   "${include_dir}"
        )
    endif ()

endfunction ()


set (
    debug_print_target_props FALSE CACHE BOOL
    "Enable debug messages printing all cmake target properties."
)

function (check_valid_target TARGET)
    cmake_parse_arguments (ARG "" "ERR_MESSAGE" "" ${ARGN})

    if (NOT TARGET ${TARGET})
        if (ERR_MESSAGE)
            message (FATAL_ERROR "${ARG_ERR_MESSAGE}")
        else ()
            message (FATAL_ERROR "${TARGET} is not a valid target.")
        endif()
    elseif (debug_print_target_props)
        # https://stackoverflow.com/questions/32183975/how-to-print-all-the-properties-of-a-target-in-cmake
        get_target_property (target_type ${TARGET} TYPE)
        if (target_type STREQUAL "INTERFACE_LIBRARY")
            set (PROP_LIST ${CMAKE_WHITELISTED_PROPERTY_LIST})
        else ()
            set (PROP_LIST ${CMAKE_PROPERTY_LIST})
        endif ()

        foreach (prop ${PROP_LIST})
            string (REPLACE "<CONFIG>" "${CMAKE_BUILD_TYPE}" prop ${prop})
            get_property (propval TARGET ${TARGET} PROPERTY ${prop} SET)
            if (propval)
                get_target_property (propval ${TARGET} ${prop})
                message ("[${TARGET}] ${prop} = ${propval}")
            endif ()
        endforeach()

    endif ()
endfunction ()
