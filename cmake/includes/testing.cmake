option(ENABLE_TESTS "Build and enable project tests." OFF)

if(ENABLE_TESTS)
    enable_testing()

    add_subdirectory("${CMAKE_SOURCE_DIR}/common/test")

    add_test(
        NAME
            common
        COMMAND
            common_tests
        WORKING_DIRECTORY
            "${CMAKE_BINARY_DIR}/test/common" )

endif()
