#ifndef GLPP_ATTRIBUTE_H
#define GLPP_ATTRIBUTE_H

#include <GL/glew.h>

#include <string>
#include <vector>

namespace glpp
{
class attribute
{
public:
    attribute( GLint              location,
               const std::string& name );

    attribute( attribute&& move ) noexcept;

    attribute( const attribute& copy ) = delete;

    virtual ~attribute() = default;

    attribute& operator=( attribute&& move ) noexcept;

    attribute& operator=( const attribute& copy ) = delete;


    GLint       location() const;

    std::string name() const;

private:
    GLint _location;

    std::string _name;
};
} // namespace glpp

#endif // ifndef GLPP_ATTRIBUTE_H
