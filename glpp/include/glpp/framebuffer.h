#ifndef GLPP_FRAMEBUFFER_H
#define GLPP_FRAMEBUFFER_H

#include "texture_2d.h"

namespace glpp
{
class framebuffer
{
public:
    framebuffer();

    explicit framebuffer( GLuint handle );

    framebuffer( framebuffer&& move ) noexcept;

    framebuffer( const framebuffer& copy ) = delete;

    virtual ~framebuffer();

    framebuffer& operator=( framebuffer&& move ) noexcept;

    framebuffer& operator=( const framebuffer& copy ) = delete;

    framebuffer& bind();

    static void  unbind();

    GLuint       handle();

    framebuffer& attach_color ( const texture_2d& texture,
                                GLint             mipmap_level = 0 );

    framebuffer& attach_depth ( const texture_2d& texture,
                                GLint             mipmap_level = 0 );

    static GLenum      check_status();

    static framebuffer defaultt();

private:
    GLuint _handle;
};
} // namespace glpp

#endif // ifndef GLPP_FRAMEBUFFER_H
