#ifndef GLPP_VERTEX_BUFFER_H
#define GLPP_VERTEX_BUFFER_H

#include "mbuffer.h"

namespace glpp
{
using vbo           = mbuffer<GL_ARRAY_BUFFER>;
using vertex_buffer = mbuffer<GL_ARRAY_BUFFER>;
} // namespace glpp

#endif // ifndef GLPP_VERTEX_BUFFER_H
