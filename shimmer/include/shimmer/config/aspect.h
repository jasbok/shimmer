#ifndef SHIMMER_CONFIG_ASPECT_H
#define SHIMMER_CONFIG_ASPECT_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
enum class aspect {
    custom,
    original,
    stretch,
    zoom
};

std::string to_string ( const enum aspect& aspect );

void        from_string ( enum aspect&       aspect,
                          const std::string& str );

void to_json ( nlohmann::json&    json,
               const enum aspect& aspect );

void from_json ( const nlohmann::json& json,
                 enum aspect&          aspect );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_ASPECT_H
