#ifndef SHIMMER_CONFIG_BLEND_H
#define SHIMMER_CONFIG_BLEND_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
enum class blend {
    none,
    add,
    subtract,
    reverse_subtract,
    min,
    max
};

std::string to_string ( const enum blend& blend );

void        from_string ( enum blend&        blend,
                          const std::string& str );

void to_json ( nlohmann::json&   json,
               const enum blend& blend );

void from_json ( const nlohmann::json& json,
                 enum blend&           blend );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_BLEND_H
