#ifndef SHIMMER_CONFIG_FILTER_H
#define SHIMMER_CONFIG_FILTER_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
enum class filter {
    linear,
    nearest
};

std::string to_string ( const enum filter& filter );

void        from_string ( enum filter&       filter,
                          const std::string& str );

void to_json ( nlohmann::json&    json,
               const enum filter& filter );

void from_json ( const nlohmann::json& json,
                 enum filter&          filter );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_FILTER_H
