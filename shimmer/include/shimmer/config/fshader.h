#ifndef SHIMMER_CONFIG_FSHADER_H
#define SHIMMER_CONFIG_FSHADER_H

#include "resource.h"

namespace shimmer::config
{
struct fshader : public resource {
    fshader();

    fshader( const std::string& ref,
             const std::string& path );
};
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_FSHADER_H
