#ifndef SHIMMER_CONFIG_GENERAL_H
#define SHIMMER_CONFIG_GENERAL_H

#include "system.h"

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct general {
    std::vector<std::string> config_dirs = system::config_dirs;
    std::vector<std::string> data_dirs   = system::data_dirs;
    std::vector<std::string> font_dirs   = system::font_dirs;
    std::vector<std::string> image_dirs  = system::image_dirs;
    std::vector<std::string> shader_dirs = system::shader_dirs;
};

void to_json ( nlohmann::json&       json,
               const struct general& general );

void from_json ( const nlohmann::json& json,
                 struct general&       general );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_GENERAL_H
