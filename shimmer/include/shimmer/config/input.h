#ifndef SHIMMER_CONFIG_INPUT_H
#define SHIMMER_CONFIG_INPUT_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct input {
    bool grab = false;
};

void to_json ( nlohmann::json&     json,
               const struct input& input );

void from_json ( const nlohmann::json& json,
                 struct input&         input );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_INPUT_H
