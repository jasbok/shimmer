#ifndef SHIMMER_CONFIG_LIMITER_H
#define SHIMMER_CONFIG_LIMITER_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct limiter {
    unsigned int rate    = 0;
    unsigned int samples = 5;
};

void to_json ( nlohmann::json&       json,
               const struct limiter& limiter );

void from_json ( const nlohmann::json& json,
                 struct limiter&       limiter );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_LIMITER_H
