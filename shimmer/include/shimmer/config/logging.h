#ifndef SHIMMER_CONFIG_LOGGING_H
#define SHIMMER_CONFIG_LOGGING_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct logging {
    enum class level {
        trace,
        debug,
        info,
        warning,
        error,
        fatal,
        off
    };
    enum level level = level::warning;

    enum class output {
        console,
        file
    };
    enum output output = output::console;

    std::string file = "";
};

std::string to_string ( const enum config::logging::level& level );

std::string to_string ( const enum config::logging::output& output );

void        from_string ( enum config::logging::level& level,
                          const std::string&           str );

void from_string ( enum config::logging::output& output,
                   const std::string&            str );

void to_json ( nlohmann::json&               json,
               const struct config::logging& logging );

void from_json ( const nlohmann::json&   json,
                 struct config::logging& logging );

void to_json ( nlohmann::json&                    json,
               const enum config::logging::level& level );

void from_json ( const nlohmann::json&        json,
                 enum config::logging::level& level );

void to_json ( nlohmann::json&                     json,
               const enum config::logging::output& output );

void from_json ( const nlohmann::json&         json,
                 enum config::logging::output& output );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_LOGGING_H
