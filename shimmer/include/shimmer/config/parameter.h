#ifndef SHIMMER_CONFIG_PARAMETER_H
#define SHIMMER_CONFIG_PARAMETER_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct parameter {
    std::string variable;
    std::string value;
};

void to_json ( nlohmann::json&         json,
               const struct parameter& parameter );

void from_json ( const nlohmann::json& json,
                 struct parameter&     parameter );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_PARAMETER_H
