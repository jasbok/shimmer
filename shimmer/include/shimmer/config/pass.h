#ifndef SHIMMER_CONFIG_PASS_H
#define SHIMMER_CONFIG_PASS_H

#include "blend.h"
#include "parameter.h"
#include "program.h"
#include "sampler.h"

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct pass {
    struct program         program;
    std::vector<parameter> uniforms;
    std::vector<sampler>   samplers = { sampler() };
    enum blend             blend    = blend::none;
};

void to_json ( nlohmann::json&    json,
               const struct pass& pass );

void from_json ( const nlohmann::json& json,
                 struct pass&          pass );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_PASS_H
