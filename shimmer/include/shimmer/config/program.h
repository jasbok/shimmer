#ifndef SHIMMER_CONFIG_PROGRAM_H
#define SHIMMER_CONFIG_PROGRAM_H

#include "fshader.h"
#include "vshader.h"

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct program {
    std::string          ref      = "default";
    std::vector<vshader> vshaders = { vshader() };
    std::vector<fshader> fshaders = { fshader() };
};

void to_json ( nlohmann::json&       json,
               const struct program& program );

void from_json ( const nlohmann::json& json,
                 struct program&       program );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_PROGRAM_H
