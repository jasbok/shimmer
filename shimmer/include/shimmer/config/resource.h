#ifndef SHIMMER_CONFIG_RESOURCE_H
#define SHIMMER_CONFIG_RESOURCE_H

#include "nlohmann/json.hpp"

#include <string>

namespace shimmer::config
{
struct resource {
    std::string ref;
    std::string path;

    resource() = default;

    resource( const std::string& ref,
              const std::string& path )
        : ref ( ref ), path ( path )
    {}
};

void to_json ( nlohmann::json&        json,
               const struct resource& resource );

void from_json ( const nlohmann::json& json,
                 struct resource&      resource );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_RESOURCE_H
