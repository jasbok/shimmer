#ifndef SHIMMER_CONFIG_SAMPLER_H
#define SHIMMER_CONFIG_SAMPLER_H

#include "filter.h"
#include "texture.h"

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct sampler {
    std::string    name         = "application";
    int            texture_unit = -1;
    struct texture texture;
    enum filter    filter = filter::nearest;
};

void to_json ( nlohmann::json&       json,
               const struct sampler& sampler );

void from_json ( const nlohmann::json& json,
                 struct sampler&       sampler );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_SAMPLER_H
