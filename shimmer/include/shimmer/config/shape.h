#ifndef SHIMMER_CONFIG_SHAPE_H
#define SHIMMER_CONFIG_SHAPE_H

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct shape {
    enum class type {
        lens,
        rectangle
    };
    enum type type = type::rectangle;

    struct lens {
        unsigned int quality = 32;
        float        curve   = 0.1f;
    };
    struct lens lens;
};

std::string to_string ( const enum shape::type& shape );

void        from_string ( enum shape::type&  shape,
                          const std::string& str );

void to_json ( nlohmann::json&     json,
               const struct shape& shape );

void from_json ( const nlohmann::json& json,
                 struct shape&         shape );

void to_json ( nlohmann::json&           json,
               const struct shape::lens& lens );

void from_json ( const nlohmann::json& json,
                 struct shape::lens&   lens );

void to_json ( nlohmann::json&         json,
               const enum shape::type& type );

void from_json ( const nlohmann::json& json,
                 enum shape::type&     type );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_SHAPE_H
