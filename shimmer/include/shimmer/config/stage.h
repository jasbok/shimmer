#ifndef SHIMMER_CONFIG_STAGE_H
#define SHIMMER_CONFIG_STAGE_H

#include "pass.h"

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct stage {
    std::string       ref    = "default";
    float             scale  = 1.0f;
    std::vector<pass> passes = { pass() };
};

void to_json ( nlohmann::json&     json,
               const struct stage& stage );

void from_json ( const nlohmann::json& json,
                 struct stage&         stage );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_STAGE_H
