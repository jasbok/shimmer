#ifndef SHIMMER_CONFIG_SURFACE_H
#define SHIMMER_CONFIG_SURFACE_H

#include "stage.h"

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct surface {
    std::string ref = "default";

    std::vector<stage> stages = { stage() };
};

void to_json ( nlohmann::json&       json,
               const struct surface& surface );

void from_json ( const nlohmann::json& json,
                 struct surface&       surface );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_SURFACE_H
