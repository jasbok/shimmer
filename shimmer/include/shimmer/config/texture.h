#ifndef SHIMMER_CONFIG_TEXTURE_H
#define SHIMMER_CONFIG_TEXTURE_H

#include "resource.h"

namespace shimmer::config
{
struct texture : public resource {
    texture()
        : resource ( "application", "" )
    {}
};
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_TEXTURE_H
