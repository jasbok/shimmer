#ifndef SHIMMER_CONFIG_VIDEO_H
#define SHIMMER_CONFIG_VIDEO_H

#include "aspect.h"
#include "filter.h"
#include "limiter.h"
#include "program.h"
#include "shape.h"
#include "surface.h"

#include "common/dims.h"

#include "nlohmann/json.hpp"

namespace shimmer::config
{
struct video {
    enum aspect aspect = aspect::original;

    common::dims_2u custom_aspect = { 16, 9 };

    struct limiter limiter;

    struct shape shape;

    std::vector<resource> textures;

    std::vector<vshader> vshaders;

    std::vector<fshader> fshaders;

    std::vector<program> programs;

    std::vector<surface> surfaces = { surface() };

    struct program shader;

    unsigned int scale = 1;

    enum filter filter = filter::nearest;
};

void to_json ( nlohmann::json&             json,
               const struct config::video& video );

void from_json ( const nlohmann::json& json,
                 struct config::video& video );
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_VIDEO_H
