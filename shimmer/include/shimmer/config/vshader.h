#ifndef SHIMMER_CONFIG_VSHADER_H
#define SHIMMER_CONFIG_VSHADER_H

#include "resource.h"

namespace shimmer::config
{
struct vshader : public resource {
    vshader();
};
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_VSHADER_H
