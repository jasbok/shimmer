#ifndef SHIMMER_CONFIGURATION_H
#define SHIMMER_CONFIGURATION_H

#include "config/general.h"
#include "config/input.h"
#include "config/logging.h"
#include "config/system.h"
#include "config/video.h"

#include "common/logger.h"

namespace shimmer
{
class configuration
{
public:
    static const common::logger& logger;

    struct config::general general;

    struct config::input input;

    struct config::logging logging;

    struct config::video video;

    /**
     * @brief merge Attempts to merge config a into b.
     * @param a The source json config object.
     * @param b The destination json config object.
     * @return The merged json config object.
     */
    static nlohmann::json merge ( const nlohmann::json& a,
                                  const nlohmann::json& b );

    /**
     * @brief from_environment Loads all config properties set through
     * environment variables.
     * @return A map of all properties set through the environment.
     */
    static nlohmann::json from_environment();

    /**
     * @brief from_file Loads all config properties set through the given config
     * file.
     * @param path Path to the config file to load.
     * @return A map of all properties set through in the config file.
     */
    static nlohmann::json from_file ( const std::string& path );

    /**
     * @brief create Creates a new config by merging the default config with
     * properties set in the environment and through a config file.
     * @return The merged config.
     */
    static configuration create();
};

void to_json ( nlohmann::json&             json,
               const struct configuration& config );

void from_json ( const nlohmann::json& json,
                 struct configuration& config );
} // namespace shimmer

#endif // ifndef SHIMMER_CONFIGURATION_H
