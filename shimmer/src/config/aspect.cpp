#include "config/aspect.h"

#include "mapping_ex.h"

namespace shimmer::config
{
std::string to_string ( const enum aspect& aspect )
{
    switch ( aspect ) {
    case aspect::custom: return "custom";

    case aspect::original: return "original";

    case aspect::stretch: return "stretch";

    case aspect::zoom: return "zoom";
    }

    return "original";
}

void from_string ( enum aspect&       aspect,
                   const std::string& str ) {
    if ( str.compare ( "custom" ) == 0 ) {
        aspect = aspect::custom;
    }
    else if ( str.compare ( "original" ) == 0 ) {
        aspect = aspect::original;
    }
    else if ( str.compare ( "stretch" ) == 0 ) {
        aspect = aspect::stretch;
    }
    else if ( str.compare ( "zoom" ) == 0 ) {
        aspect = aspect::zoom;
    }
    else {
        throw mapping_exception ( "aspect", str,
                                  { "custom",
                                    "original",
                                    "stretch",
                                    "zoom" } );
    }
}

void to_json ( nlohmann::json&    json,
               const enum aspect& aspect ) {
    json = to_string ( aspect );
}

void from_json ( const nlohmann::json& json,
                 enum aspect&          aspect ) {
    from_string ( aspect, json );
}
} // namespace shimmer::config
