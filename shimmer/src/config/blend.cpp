#include "config/blend.h"

#include "mapping_ex.h"

namespace shimmer::config
{
std::string to_string ( const blend& blend )
{
    switch ( blend ) {
    case blend::add: return "add";

    case blend::max: return "max";

    case blend::min: return "min";

    case blend::none: return "none";

    case blend::reverse_subtract: return "reverse_subtract";

    case blend::subtract: return "subtract";
    }

    return "none";
}

void from_string ( blend& blend, const std::string& str )
{
    if ( str.compare ( "add" ) == 0 ) {
        blend = blend::add;
    }
    else if ( str.compare ( "max" ) == 0 ) {
        blend = blend::max;
    }
    else if ( str.compare ( "min" ) == 0 ) {
        blend = blend::min;
    }
    else if ( str.compare ( "none" ) == 0 ) {
        blend = blend::none;
    }
    else if ( str.compare ( "reverse_subtract" ) == 0 ) {
        blend = blend::reverse_subtract;
    }
    else if ( str.compare ( "subtract" ) == 0 ) {
        blend = blend::subtract;
    }
    else {
        throw mapping_exception ( "blend", str,
                                  { "add",
                                    "max",
                                    "min",
                                    "none",
                                    "reverse_subtract",
                                    "subtract" } );
    }
}

void to_json ( nlohmann::json&   json,
               const enum blend& blend ) {
    json = to_string ( blend );
}

void from_json ( const nlohmann::json& json,
                 enum blend&           blend ) {
    from_string ( blend, json );
}
} // namespace shimmer::config
