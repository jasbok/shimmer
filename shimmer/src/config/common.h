#ifndef SHIMMER_CONFIG_COMMON_H
#define SHIMMER_CONFIG_COMMON_H

#include "common/dims.h"
#include "common/json.h"

#include "nlohmann/json.hpp"

namespace common
{
template<typename T>
void to_json ( nlohmann::json&          json,
               const common::dims_2<T>& dims ) {
    json = {
        { "width",  dims.width  },
        { "height", dims.height }
    };
}

template<typename T>
void from_json ( const nlohmann::json& json,
                 common::dims_2<T>&    dims ) {
    if ( common::json::exists ( json, { "width" } ) ) {
        dims.width = json.at ( "width" );
    }

    if ( common::json::exists ( json, { "height" } ) ) {
        dims.height = json.at ( "height" );
    }
}
} // namespace common

#endif // ifndef SHIMMER_CONFIG_COMMON_H
