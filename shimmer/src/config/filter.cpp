#include "config/filter.h"

#include "mapping_ex.h"

namespace shimmer::config
{
std::string to_string ( const enum filter& filter )
{
    switch ( filter ) {
    case filter::linear: return "linear";

    case filter::nearest: return "nearest";
    }

    return "nearest";
}

void from_string ( enum filter&       filter,
                   const std::string& str ) {
    if ( str.compare ( "linear" ) == 0 ) {
        filter = filter::linear;
    }
    else if ( str.compare ( "nearest" ) == 0 ) {
        filter = filter::nearest;
    }
    else {
        throw mapping_exception ( "filter", str,
                                  { "linear",
                                    "nearest" } );
    }
}

void to_json ( nlohmann::json&    json,
               const enum filter& filter ) {
    json = to_string ( filter );
}

void from_json ( const nlohmann::json& json,
                 enum filter&          filter ) {
    from_string ( filter, json );
}
} // namespace shimmer::config
