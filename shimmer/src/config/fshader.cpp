#include "config/fshader.h"

namespace shimmer::config
{
fshader::fshader()
    : resource ( "default", "default.frag" )
{}

fshader::fshader( const std::string& ref,
                  const std::string& path )
    : resource ( ref, path )
{}
} // namespace shimmer::config
