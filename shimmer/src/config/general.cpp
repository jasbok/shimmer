#include "config/general.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&       json,
               const struct general& general ) {
    json = {
        { "config_dirs",  general.config_dirs },
        { "data_dirs",    general.data_dirs   },
        { "font_dirs",    general.font_dirs   },
        { "image_dirs",   general.image_dirs  },
        { "shaders_dirs", general.shader_dirs },
    };
}

void from_json ( const nlohmann::json& json,
                 struct general&       general ) {
    utils::set_property ( general.config_dirs, json, "config_dirs" );
    utils::set_property ( general.data_dirs,   json, "data_dirs" );
    utils::set_property ( general.font_dirs,   json, "font_dirs" );
    utils::set_property ( general.image_dirs,  json, "image_dirs" );
    utils::set_property ( general.shader_dirs, json, "shader_dirs" );
}
} // namespace shimmer::config
