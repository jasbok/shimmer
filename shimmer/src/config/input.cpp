#include "config/input.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&             json,
               const struct config::input& input ) {
    json = {
        { "grab", input.grab }
    };
}

void from_json ( const nlohmann::json& json,
                 struct config::input& input ) {
    utils::set_property ( input.grab, json, "grab" );
}
} // namespace shimmer::config
