#include "config/limiter.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&       json,
               const struct limiter& limiter ) {
    json = {
        { "rate",    limiter.rate    },
        { "samples", limiter.samples }
    };
}

void from_json ( const nlohmann::json& json,
                 struct limiter&       limiter ) {
    utils::set_property ( limiter.rate,    json, "rate" );
    utils::set_property ( limiter.samples, json, "samples" );
}
} // namespace shimmer::config
