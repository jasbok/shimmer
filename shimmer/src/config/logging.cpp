#include "config/logging.h"

#include "mapping_ex.h"
#include "utils.h"

namespace shimmer::config
{
std::string to_string ( const enum logging::level& level )
{
    switch ( level ) {
    case logging::level::debug: return "debug";

    case logging::level::error: return "error";

    case logging::level::fatal: return "fatal";

    case logging::level::info: return "info";

    case logging::level::off: return "off";

    case logging::level::trace: return "trace";

    case logging::level::warning: return "warning";
    }

    return "warning";
}

std::string to_string ( const enum logging::output& output )
{
    switch ( output ) {
    case logging::output::console: return "console";

    case logging::output::file: return "file";
    }

    return "console";
}

void from_string ( enum logging::level& level,
                   const std::string&   str ) {
    if ( str.compare ( "debug" ) == 0 ) {
        level = logging::level::debug;
    }
    else if ( str.compare ( "error" ) == 0 ) {
        level = logging::level::error;
    }
    else if ( str.compare ( "fatal" ) ==  0 ) {
        level = logging::level::fatal;
    }
    else if ( str.compare ( "info" ) == 0 ) {
        level = logging::level::info;
    }
    else if ( str.compare ( "off" ) == 0 ) {
        level = logging::level::off;
    }
    else if ( str.compare ( "trace" ) == 0 ) {
        level = logging::level::trace;
    }
    else if ( str.compare ( "warning" ) == 0 ) {
        level = logging::level::warning;
    }
    else {
        throw mapping_exception ( "logging::level", str,
                                  { "trace",
                                    "debug",
                                    "info",
                                    "warning",
                                    "error",
                                    "fatal",
                                    "off" } );
    }
}

void from_string ( enum logging::output& output,
                   const std::string&    str ) {
    if ( str.compare ( "console" ) == 0 ) {
        output = logging::output::console;
    }
    else if ( str.compare ( "file" ) == 0 ) {
        output = logging::output::file;
    }
    else {
        throw mapping_exception ( "logging::output", str,
                                  { "console",
                                    "file" } );
    }
}

void to_json ( nlohmann::json&       json,
               const struct logging& logging ) {
    json = {
        { "level",  logging.level  },
        { "output", logging.output },
        { "file",   logging.file   }
    };
}

void from_json ( const nlohmann::json& json,
                 struct logging&       logging ) {
    utils::set_property ( logging.level,  json, "level" );
    utils::set_property ( logging.output, json, "output" );
    utils::set_property ( logging.file,   json, "file" );
}

void to_json ( nlohmann::json&            json,
               const enum logging::level& level ) {
    json = to_string ( level );
}

void from_json ( const nlohmann::json& json,
                 enum logging::level&  level ) {
    from_string ( level, json );
}

void to_json ( nlohmann::json&             json,
               const enum logging::output& output ) {
    json = to_string ( output );
}

void from_json ( const nlohmann::json& json,
                 enum logging::output& output ) {
    from_string ( output, json );
}
} // namespace shimmer::config
