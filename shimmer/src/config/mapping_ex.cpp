#include "mapping_ex.h"

#include "common/str.h"

namespace shimmer::config
{
config::mapping_exception::mapping_exception(
    const std::string&              property,
    const std::string&              value,
    const std::vector<std::string>& expected )
    : runtime_error ( "Could not map value: '"
          + property + "' => '" + value
          + "'; expected one of the following: ["
          + common::str::join ( expected, ", " ) + "]." )
{}
} // namespace shimmer::config
