#ifndef SHIMMER_CONFIG_MAPPING_EX_H
#define SHIMMER_CONFIG_MAPPING_EX_H

#include <stdexcept>
#include <string>
#include <vector>

namespace shimmer::config
{
struct mapping_exception : public std::runtime_error {
    mapping_exception( const std::string&              property,
                       const std::string&              value,
                       const std::vector<std::string>& expected );

    virtual ~mapping_exception() = default;
};
} // namespace shimmer::config

#endif // ifndef SHIMMER_CONFIG_MAPPING_EX_H
