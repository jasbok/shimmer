#include "config/parameter.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&         json,
               const struct parameter& parameter )
{
    json = {
        { "value",    parameter.value    },
        { "variable", parameter.variable }
    };
}

void from_json ( const nlohmann::json& json,
                 struct parameter&     parameter )
{
    utils::set_property ( parameter.value,    json, "value" );
    utils::set_property ( parameter.variable, json, "variable" );
}
} // namespace shimmer::config
