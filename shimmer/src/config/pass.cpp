#include "config/pass.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&    json,
               const struct pass& pass )
{
    json = {
        { "program",        pass.program  },
        { "uniforms",       pass.uniforms },
        { "samplers",       pass.samplers },
        { "blend_function", pass.blend    }
    };
}

void from_json ( const nlohmann::json& json,
                 struct pass&          pass )
{
    utils::set_property ( pass.program,  json, "program" );
    utils::set_property ( pass.uniforms, json, "uniforms" );
    utils::set_property ( pass.samplers, json, "samplers" );
    utils::set_property ( pass.blend,    json, "blend" );
}
} // namespace shimmer::config
