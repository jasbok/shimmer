#include "config/program.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&       json,
               const struct program& program ) {
    json = {
        { "fragment_shaders", program.fshaders },
        { "vertex_shaders",   program.vshaders },
        { "ref",              program.ref      }
    };
}

void from_json ( const nlohmann::json& json,
                 struct program&       program ) {
    if ( json.is_object() ) {
        utils::set_property ( program.fshaders,
                              json,
                              "fshaders" );

        utils::set_property ( program.vshaders,
                              json,
                              "shaders" );

        utils::set_property ( program.ref,
                              json,
                              "ref" );
    }
    else if ( json.is_string() ) {
        utils::set_property ( program.ref,              json );
        utils::set_property ( program.fshaders[0].path, json );
    }
}
} // namespace shimmer::config
