#include "config/resource.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&        json,
               const struct resource& resource )
{
    json = {
        { "ref",  resource.ref  },
        { "path", resource.path }
    };
}

void from_json ( const nlohmann::json& json,
                 struct resource&      resource )
{
    if ( json.is_object() ) {
        utils::set_property ( resource.ref,  json, "ref" );
        utils::set_property ( resource.path, json, "path" );
    }
    else if ( json.is_string() ) {
        utils::set_property ( resource.ref,  json );
        utils::set_property ( resource.path, json );
    }
}
} // namespace shimmer::config
