#include "config/sampler.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&       json,
               const struct sampler& sampler )
{
    json = {
        { "name",    sampler.name    },
        { "texture", sampler.texture },
        { "filter",  sampler.filter  }
    };
}

void from_json ( const nlohmann::json& json,
                 struct sampler&       sampler )
{
    utils::set_property ( sampler.name,    json, "name" );
    utils::set_property ( sampler.texture, json, "texture" );
    utils::set_property ( sampler.filter,  json, "filter" );
}
} // namespace shimmer::config
