#include "config/shape.h"

#include "mapping_ex.h"
#include "utils.h"

namespace shimmer::config
{
std::string to_string ( const enum shape::type& shape ) {
    switch ( shape ) {
    case shape::type::lens: return "lens";

    case shape::type::rectangle: return "rectangle";
    }

    return "rectangle";
}

void from_string ( enum shape::type&  shape,
                   const std::string& str ) {
    if ( str.compare ( "lens" ) == 0 ) {
        shape = shape::type::lens;
    }
    else if ( str.compare ( "rectangle" ) == 0 ) {
        shape = shape::type::rectangle;
    }
    else {
        throw mapping_exception ( "shape::type", str,
                                  { "lens",
                                    "rectangle" } );
    }
}

void to_json ( nlohmann::json&     json,
               const struct shape& shape ) {
    json = {
        { "type", shape.type },
        { "lens", shape.lens }
    };
}

void from_json ( const nlohmann::json& json,
                 struct shape&         shape ) {
    utils::set_property ( shape.lens, json, "lens" );
    utils::set_property ( shape.type, json, "type" );
}

void to_json ( nlohmann::json&           json,
               const struct shape::lens& lens ) {
    json = {
        { "curve",   lens.curve   },
        { "quality", lens.quality }
    };
}

void from_json ( const nlohmann::json& json,
                 struct shape::lens&   lens ) {
    utils::set_property ( lens.curve,   json, "curve" );
    utils::set_property ( lens.quality, json, "quality" );
}

void to_json ( nlohmann::json&         json,
               const enum shape::type& type ) {
    json = to_string ( type );
}

void from_json ( const nlohmann::json& json,
                 enum shape::type&     type ) {
    from_string ( type, json );
}
} // namespace shimmer::config
