#include "config/stage.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&     json,
               const struct stage& stage )
{
    json = {
        { "ref",    stage.ref    },
        { "scale",  stage.scale  },
        { "passes", stage.passes }
    };
}

void from_json ( const nlohmann::json& json,
                 struct stage&         stage )
{
    utils::set_property ( stage.ref,    json, "ref" );
    utils::set_property ( stage.scale,  json, "scale" );
    utils::set_property ( stage.passes, json, "passes" );
}
} // namespace shimmer::config
