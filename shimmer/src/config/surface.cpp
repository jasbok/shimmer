#include "config/surface.h"

#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&       json,
               const struct surface& surface )
{
    json = {
        { "ref",    surface.ref    },
        { "stages", surface.stages }
    };
}

void from_json ( const nlohmann::json& json,
                 struct surface&       surface )
{
    utils::set_property ( surface.ref,    json, "ref" );
    utils::set_property ( surface.stages, json, "stages" );
}
} // namespace shimmer::config
