#include "utils.h"

#include "fmt/format.h"

namespace shimmer::utils
{
void set_property ( unsigned int&         property,
                    const nlohmann::json& json,
                    const std::string&    field ) {
    try {
        auto value = json.at ( field );

        if ( value.is_string() ) {
            value = std::stof ( value.get<std::string>() );
        }

        property = value.get<unsigned int>();
    }
    catch ( const nlohmann::json::exception& jex ) {
        if ( jex.id == 403 ) {}
        else {
            throw property_exception (
                      fmt::format (
                          "Unable to set config property: {} -> {}\n{}",
                          field,
                          json.at ( field ).dump(),
                          jex.what() ) );
        }
    }
    catch ( const std::exception& ex  ) {
        throw property_exception (
                  fmt::format (
                      "Unable to set config property: {} -> {}\nExpected an unsigned integer.\nException: {}",
                      field,
                      json.at ( field ).dump(),
                      ex.what() ) );
    }
}

void set_property ( float&                property,
                    const nlohmann::json& json,
                    const std::string&    field ) {
    try {
        auto value = json.at ( field );

        if ( value.is_string() ) {
            value = std::stof ( value.get<std::string>() );
        }

        property = value.get<float>();
    }
    catch ( const nlohmann::json::exception& jex ) {
        if ( jex.id == 403 ) {}
        else {
            throw property_exception (
                      fmt::format (
                          "Unable to set config property: {} -> {}\n{}",
                          field,
                          json.at ( field ).dump(),
                          jex.what() ) );
        }
    }
    catch ( const std::exception& ex  ) {
        throw property_exception (
                  fmt::format (
                      "Unable to set config property: {} -> {}\nExpected a float.\nException: {}",
                      field,
                      json.at ( field ).dump(),
                      ex.what() ) );
    }
}

property_exception::property_exception(
    const std::string& message )
    : runtime_error ( message )
{}
} // namespace shimmer
