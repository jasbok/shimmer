#ifndef SHIMMER_CONFIG_UTILS_H
#define SHIMMER_CONFIG_UTILS_H

#include "common/logger.h"

#include "nlohmann/json.hpp"

#include <string>

namespace shimmer::utils
{
struct property_exception : public std::runtime_error {
    property_exception( const std::string& message );

    virtual ~property_exception() = default;
};

template<typename T>
void set_property ( T&                    property,
                    const nlohmann::json& json,
                    const std::string&    field ) {
    try {
        property = json.at ( field ).get<T>();
    }
    catch ( const nlohmann::json::exception& ex ) {
        if ( ex.id != 403 ) {
            throw property_exception (
                      fmt::format (
                          "Unable to set config property: {} -> {}\nException: {}",
                          field,
                          json.at ( field ).dump(),
                          ex.what() ) );
        }
        else {
            // Property was not defined, leave as default.
        }
    }
    catch ( const std::exception& ex ) {
        throw property_exception (
                  fmt::format (
                      "Unable to set config property: {} -> {}\nException: {}",
                      field,
                      json.at ( field ).dump(),
                      ex.what() ) );
    }
}

template<typename T>
void set_property ( T&                    property,
                    const nlohmann::json& json ) {
    try {
        property = json.get<T>();
    }
    catch ( const nlohmann::json::exception& ex ) {
        if ( ex.id != 403 ) {
            throw property_exception (
                      fmt::format (
                          "Unable to set config property: {}\nException: {}",
                          json.dump(),
                          ex.what() ) );
        }
        else {
            // Property was not defined, leave as default.
        }
    }
    catch ( const std::exception& ex ) {
        throw property_exception (
                  fmt::format (
                      "Unable to set config property: {}\nException: {}",
                      json.dump(),
                      ex.what() ) );
    }
}

void set_property ( unsigned int&         property,
                    const nlohmann::json& json,
                    const std::string&    field );

void set_property ( float&                property,
                    const nlohmann::json& json,
                    const std::string&    field );
} // namespace shimmer

#endif // ifndef SHIMMER_CONFIG_UTILS_H
