#include "config/video.h"

#include "common.h"
#include "utils.h"

namespace shimmer::config
{
void to_json ( nlohmann::json&     json,
               const struct video& video ) {
    json = {
        { "aspect",        video.aspect        },
        { "custom_aspect", video.custom_aspect },
        { "limiter",       video.limiter       },
        { "shape",         video.shape         },

        { "textures",      video.textures      },
        { "vshaders",      video.vshaders      },
        { "fshaders",      video.fshaders      },
        { "programs",      video.programs      },
        { "surfaces",      video.surfaces      },

        { "filter",        video.filter        },
        { "scale",         video.scale         },
        { "shader",        video.shader        }
    };
}

void from_json ( const nlohmann::json& json,
                 struct video&         video ) {
    utils::set_property ( video.aspect,        json, "aspect" );
    utils::set_property ( video.custom_aspect, json, "custom_aspect" );
    utils::set_property ( video.limiter,       json, "limiter" );
    utils::set_property ( video.shape,         json, "shape" );

    utils::set_property ( video.textures,      json, "textures" );
    utils::set_property ( video.vshaders,      json, "vshaders" );
    utils::set_property ( video.fshaders,      json, "fshaders" );
    utils::set_property ( video.programs,      json, "programs" );
    utils::set_property ( video.surfaces,      json, "surfaces" );

    utils::set_property ( video.filter,        json, "filter" );
    utils::set_property ( video.scale,         json, "scale" );
    utils::set_property ( video.shader,        json, "shader" );
}
} // namespace shimmer::config
