#include "configuration.h"

#include "common/env.h"
#include "common/file.h"
#include "common/json.h"
#include "common/logger.h"

#include "config/utils.h"

#include <sstream>

namespace shimmer
{
const common::logger& configuration::logger =
    common::logger::get ( "shimmer::configuration" );

nlohmann::json configuration::merge ( const nlohmann::json& a,
                                      const nlohmann::json& b )
{
    auto merged = b.flatten();

    for ( const auto& prop : a.items() ) {
        if ( !prop.value().is_null() ) merged[prop.key()] = prop.value();
    }

    return merged.unflatten();
}

nlohmann::json configuration::from_environment()
{
    nlohmann::json conf = nlohmann::json::object();

    auto evars = common::env::find_all ( std::regex ( "^SHIMMER_.*" ) );

    if ( !evars.empty() ) {
        for ( auto& evar : evars ) {
            auto key = evar.first.substr ( 8, evar.first.length() );
            key = common::str::replace ( key, "_", "/" );
            key = common::str::replace ( key, "//", "_" );
            key = common::str::lower ( key );

            conf["/" + key] = evar.second;
        }

        conf = conf.unflatten();
    }

    logger.debug ( "Environment config: {}", conf.dump ( 2 ) );

    return conf;
}

nlohmann::json configuration::from_file ( const std::string& path )
{
    auto conf = nlohmann::json ( common::file::read_all ( path ) );

    logger.debug ( "File config ({}): {}", path, conf.dump ( 2 ) );

    return nlohmann::json ( conf );
}

configuration configuration::create()
{
    configuration conf;

    try {
        conf = from_environment();
    }
    catch ( const std::exception& ex ) {
        logger.error ( "Unable to parse environment configuration:\n{}",
                       ex.what() );
    }
    try {
        if ( !conf.general.config_dirs.empty() ) {
            try {
                auto config_file = common::file::find ( "shimmer.conf",
                                                        conf.general.config_dirs );

                conf = merge ( from_file ( config_file ), conf );
            }
            catch ( const std::exception& ex ) {
                logger.warn ( "Failed to load config file:\n{}", ex.what() );
            }
        }
    }
    catch ( const std::exception& ex ) {
        logger.error ( "Unable to parse config file configuration:\n{}",
                       ex.what() );
    }
    logger.debug ( "Shimmer configuration: {}",
                   nlohmann::json ( conf ).dump ( 2 ) );

    return conf;
}

void to_json ( nlohmann::json&      json,
               const configuration& config ) {
    json = {
        { "general", config.general },
        { "input",   config.input   },
        { "logging", config.logging },
        { "video",   config.video   },
    };
}

void from_json ( const nlohmann::json& json,
                 configuration&        config ) {
    utils::set_property ( config.general, json, "general" );
    utils::set_property ( config.input,   json, "input" );
    utils::set_property ( config.logging, json, "logging" );
    utils::set_property ( config.video,   json, "video" );
}
} // namespace shimmer
