#include "renderer.h"

#include "common/env.h"
#include "common/file.h"
#include "common/str.h"

#include "glpp/debug.h"
#include "glpp/shader.h"
#include "glpp/shapes.h"
#include "glpp/vertex_attrib.h"
#include "glpp/viewport.h"

#include "shimmer/video.h"

namespace shimmer
{
const common::logger& renderer::logger =
    common::logger::get ( "shimmer::renderer" );

void renderer::init() {
    glewExperimental = GL_TRUE;
    GLenum glew_err = glewInit();

    if ( glew_err ) {
        logger.fatal ( "Unable to initialise GLEW: {}",
                       glewGetErrorString ( glew_err ) );
    }
    else {
        logger.info ( "Successfully initialised GLEW." );
    }
}

renderer::renderer( struct configuration* conf )
    : _conf ( conf ),
      _flip_target ( false )
{
    _pipeline.resize ( 2 );

    _pipeline.front().scale = conf->video.scale;
    _pipeline.back().fbo    = glpp::framebuffer::defaultt();

    _define_program ( _pipeline.front().program,
                      _conf->video.shader,
                      1 );

    _define_program ( _pipeline.back().program,
                      config::program(),
                      2 );

    _define_vbo ( _pipeline.front().vbo,
                  glpp::shapes::quad()
                      .flip_y ( _flip_target )
                      .position_texcoord() );

    _define_ebo ( _pipeline.front().ebo,
                  glpp::shapes::quad::triangle_fan_indices );

    _define_vbo ( _pipeline.back().vbo,
                  _shape_position_texcoords() );

    _define_ebo ( _pipeline.back().ebo,
                  _shape_indices() );

    _define_vao ( _pipeline.front().vao,
                  _pipeline.front().vbo,
                  _pipeline.front().ebo,
                  _pipeline.front().program );

    _define_vao ( _pipeline.back().vao,
                  _pipeline.back().vbo,
                  _pipeline.back().ebo,
                  _pipeline.back().program );

    if ( _conf->video.filter == config::filter::linear ) {
        _texture_filter = glpp::texture_2d::filter::linear;
    }
    else {
        _texture_filter = glpp::texture_2d::filter::nearest;
    }
}

void renderer::source_resolution ( const common::dims_2u& dims ) {
    _source_resolution = dims;

    _calculate_aspect();
    _define_vbo ( _pipeline.front().vbo,
                  glpp::shapes::quad()
                      .flip_y ( _flip_target )
                      .position_texcoord() );

    _define_texture ( _source_tex,
                      _source_resolution,
                      glpp::texture_2d::filter::nearest,
                      1 );
    _source_fbo.bind().attach_color ( _source_tex );

    if ( _pipeline.size() > 1 ) {
        _pipeline.front().resolution = dims * _pipeline.front().scale;

        _define_texture ( _target_tex,
                          _pipeline.front().resolution,
                          _texture_filter,
                          2 );
        _pipeline.front().fbo.bind().attach_color ( _target_tex ).unbind();
    }
}

void renderer::target_resolution ( const common::dims_2u& dims )
{
    _target_resolution = dims;

    _pipeline.back().resolution = dims;

    _calculate_aspect();

    _define_vbo ( _pipeline.back().vbo,
                  _shape_position_texcoords() );
}

void renderer::render() {
    GLPP_CHECK_ERROR_AND_FRAMEBUFFER ( "PRE RENDER" );

    for ( auto& pass : _pipeline ) {
        pass.fbo.bind();
        GLPP_CHECK_ERROR ( "RENDERER: Bound target FBO" );

        glpp::viewport ( pass.resolution );

        pass.program.use();
        pass.vao.bind();

        glClear ( GL_COLOR_BUFFER_BIT );
        glDrawElements ( GL_TRIANGLE_FAN,
                         static_cast<GLint>( pass.ebo.elements() ),
                         GL_UNSIGNED_INT,
                         nullptr );
        GLPP_CHECK_ERROR ( "RENDERER: Rendered pass" );
    }

    glpp::program::unbind();

    GLPP_CHECK_ERROR_AND_FRAMEBUFFER ( "POST RENDER" );
}

void renderer::capture_fbo() {
    _source_fbo.bind();
    glpp::viewport ( _source_resolution );
}

void renderer::reset_fbo() {
    _source_fbo.unbind();
    glpp::viewport ( _target_resolution );
}

void renderer::activate_source_texture_unit()
{
    glpp::texture::active_texture ( 1 );
}

void renderer::setup_viewport()
{
    glpp::viewport ( _target_resolution );
}

void renderer::copy_source ( uint8_t*               data,
                             const common::dims_2u& dims,
                             unsigned int           channels )
{
    _source_tex.bind();
    _source_tex.sub_image ( data, dims, channels );
}

void renderer::flip_target ( bool flip )
{
    _flip_target = flip;
    _define_vbo ( _pipeline.front().vbo,
                  glpp::shapes::quad()
                      .flip_y ( _flip_target )
                      .position_texcoord() );
}

void renderer::_define_program ( glpp::program&         program,
                                 const config::program& cprogram,
                                 int                    texture_unit ) {
    logger.debug ( "Creating program..." );

    const auto& shader_dirs = _conf->general.shader_dirs;

    std::vector<glpp::shader> shaders;

    for ( const auto& vshader : cprogram.vshaders ) {
        auto vs_file   = common::file::find ( vshader.path, shader_dirs );
        auto vs_source = common::file::read_all ( vs_file );
        auto vs        = glpp::shader::create().vertex ( vs_source );
        vs.compile_and_throw();
        shaders.push_back ( std::move ( vs ) );
    }

    for ( const auto& fshader : cprogram.fshaders ) {
        auto fs_file   = common::file::find ( fshader.path, shader_dirs );
        auto fs_source = common::file::read_all ( fs_file );
        auto fs        = glpp::shader::create().fragment ( fs_source );
        fs.compile_and_throw();
        shaders.push_back ( std::move ( fs ) );
    }

    for ( const auto& shader : shaders ) {
        program.attach ( shader );
    }

    program.link_and_throw();

    for ( const auto& shader : shaders ) {
        program.detach ( shader );
    }

    program.use()
        .uniform ( "application", texture_unit )
        .unbind();

    GLPP_CHECK_ERROR ( "Created Program" );
}

void renderer::_define_vbo ( glpp::vbo&                vbo,
                             const std::vector<float>& data ) {
    logger.debug ( "Creating VBO..." );

    vbo.bind()
        .data ( data )
        .unbind();

    GLPP_CHECK_ERROR ( "Created VBO" );
}

void renderer::_define_ebo ( glpp::ebo&                       ebo,
                             const std::vector<unsigned int>& indices ) {
    logger.debug ( "Creating ebo..." );

    ebo.bind()
        .data ( indices )
        .unbind();

    GLPP_CHECK_ERROR ( "Created EBO" );
}

void renderer::_define_vao ( glpp::vao&           vao,
                             const glpp::vbo&     vbo,
                             const glpp::ebo&     ebo,
                             const glpp::program& program )
{
    logger.debug ( "Defining VAO..." );

    vao.bind();
    vbo.bind();
    ebo.bind();

    program.use();
    auto position_location = program.attribute_location ( "position" );
    auto texcoord_location = program.attribute_location ( "texcoord" );

    logger.debug ( "position location: {}", position_location );
    logger.debug ( "texcoord location: {}", texcoord_location );

    auto attribs = glpp::vertex_attrib_builder<float>::sequential ( {
            { "position", 2, position_location },
            { "texcoord", 2, texcoord_location }
        } );

    attribs[0].define_pointer().enable_array();
    attribs[1].define_pointer().enable_array();

    program.unbind();
    vao.unbind();
    vbo.unbind();
    ebo.unbind();

    GLPP_CHECK_ERROR ( "Defined VAO" );
}

void renderer::_define_texture ( glpp::texture_2d&        tex,
                                 const common::dims_2u&   dims,
                                 glpp::texture_2d::filter filter,
                                 unsigned int             texture_unit ) {
    logger.debug ( "Creating texture..." );
    logger.debug ( "Texture size: {}", dims.to_json() );

    glpp::texture::active_texture ( texture_unit );
    tex.bind();
    tex.image ( glpp::texture_2d::internal_format::rgb, dims )
        .generate_mipmaps()
        .wrap ( glpp::texture_2d::texture_wrap::clamp_to_edge )
        .filters ( filter );

    glpp::texture::active_texture ( 0 );

    GLPP_CHECK_ERROR ( "Created TEXTURE" );
}

void renderer::_calculate_aspect()
{
    _aspect = video::aspect_transform ( _source_resolution,
                                        _target_resolution,
                                        *_conf );
}

std::vector<float> renderer::_shape_position_texcoords()
{
    auto& shape = _conf->video.shape;

    switch ( shape.type ) {
    case config::shape::type::lens:

        return glpp::shapes::lens()
                   .dims ( _aspect )
                   .curve_factor ( shape.lens.curve )
                   .curve_detail ( shape.lens.quality )
                   .position_texcoord();

    case config::shape::type::rectangle:

        return glpp::shapes::quad()
                   .dims ( _aspect )
                   .position_texcoord();
    }

    throw std::exception();
}

std::vector<unsigned int> renderer::_shape_indices()
{
    auto& shape = _conf->video.shape;

    switch ( shape.type ) {
    case config::shape::type::lens:

        return glpp::shapes::lens()
                   .curve_factor ( shape.lens.curve )
                   .curve_detail ( shape.lens.quality )
                   .indices();

    case config::shape::type::rectangle:

        return glpp::shapes::quad().triangle_fan_indices;
    }

    throw std::exception();
}
} // namespace shimmer
